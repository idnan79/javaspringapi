package com.idnan.assignapi.data_access;


import com.idnan.assignapi.models.CountryCount;
import com.idnan.assignapi.models.Customer;
import com.idnan.assignapi.models.CustomerGenre;
import com.idnan.assignapi.models.CustomerSpender;

import java.util.ArrayList;

public interface CustomerRepository {
    public ArrayList<Customer> getAllCustomers();
    public Customer getCustomerById(int custId);
    public Boolean addCustomer(Customer customer);
    public Boolean updateCustomer(Customer customer);
    public  ArrayList<Customer> getCustomerByName(String name);
    public ArrayList<CountryCount> getCustomerByCountry();
    public ArrayList<Customer> GetAllCustomersFromLimitOfset(int limit, int ofset);
    public ArrayList<CustomerGenre>  GetCustomerPopularGenre(int id);
    public ArrayList<CustomerSpender>  GetAllCustomersByHighestSpender();


}