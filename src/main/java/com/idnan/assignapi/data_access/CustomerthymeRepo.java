package com.idnan.assignapi.data_access;



import com.idnan.assignapi.models.Artist;
import com.idnan.assignapi.models.Genre;
import com.idnan.assignapi.models.SearchTrack;
import com.idnan.assignapi.models.Track;

import java.util.ArrayList;

//interfaces for thymeleaf (views)
public interface CustomerthymeRepo {
    public ArrayList<Artist> getRandomArtists();
    public ArrayList<Track> getRandomTrack();
    public ArrayList<Genre> getRandomGenre();
    public  ArrayList<SearchTrack> getTrackByNameSearch(String name);
}
