package com.idnan.assignapi.models;

// Shorter version of customer
public class Customer {

    private String Company;
    private int CustomerId;
    private String FirstName;
    private String LastName;
    private String Country;
    private String PostalCode;
    private String Phone;
    private String Email;

    public Customer(){

    }

    public Customer(int customerId, String firstName, String lastName, String country, String postalCode, String phone, String email) {
    }

    public Customer(String phone, String company, int customerId, String firstName, String lastName, String country, String phone1, String email) {

        Company = company;
        CustomerId = customerId;
        FirstName = firstName;
        LastName = lastName;
        Country = country;
        this.Phone = phone;
        Email = email;
    }

    // Getters and setters


    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }

    public int getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(int customerId) {
        CustomerId = customerId;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }
}