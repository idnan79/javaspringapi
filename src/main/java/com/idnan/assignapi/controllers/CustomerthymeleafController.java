package com.idnan.assignapi.controllers;


import com.idnan.assignapi.data_access.CustomerthymeRepo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CustomerthymeleafController {
    private final CustomerthymeRepo customerthymeRepo;

    public CustomerthymeleafController(CustomerthymeRepo customerRepository) {
        this.customerthymeRepo = customerRepository;
    }
    // return  random 5Artist, 5 Trck ,5 Genre
    //search bar for traks
    @RequestMapping(value="/", method = RequestMethod.GET)
    public String getAllCustomers(Model model,String keyword){
        model.addAttribute("Artist", customerthymeRepo.getRandomArtists());
        model.addAttribute("Trck", customerthymeRepo.getRandomTrack());
        model.addAttribute("Genre", customerthymeRepo.getRandomGenre());
        model.addAttribute("Trcko", customerthymeRepo.getTrackByNameSearch(keyword));
        return "view-customers";

    }
}
