package com.idnan.assignapi.controllers;

import com.idnan.assignapi.data_access.CustomerRepository;
import com.idnan.assignapi.models.CountryCount;
import com.idnan.assignapi.models.Customer;

import com.idnan.assignapi.models.CustomerGenre;
import com.idnan.assignapi.models.CustomerSpender;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class CustomerController {
    // Configure some endpoints
    private final CustomerRepository customerRepository;

    public CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }
    // get all  customers
    @RequestMapping(value="/api/customers", method = RequestMethod.GET)
    public ArrayList<Customer> getAllCustomers(){
        return customerRepository.getAllCustomers();
    }
    // get  customer by id
    @RequestMapping(value = "api/customers/id/{id}", method = RequestMethod.GET)
    public Customer getCustomerById(@PathVariable int id){
        return customerRepository.getCustomerById(id);
    }
    // get  customers by ofset and limit
    @RequestMapping(value = "api/customers/Ofset/{Ofset}/Limit/{Limit}", method = RequestMethod.GET)
    public ArrayList<Customer> GetAllCustomersFromLimitOfset(@PathVariable int Ofset, @PathVariable int Limit){
        return customerRepository.GetAllCustomersFromLimitOfset(Ofset, Limit);
    }
    // get all  customers by name using like keyword for example if you search n all cusomers name  start with n will be display
    @RequestMapping(value = "api/customers/name/{Name}", method = RequestMethod.GET)
    public ArrayList<Customer> getCustomerByName(@PathVariable String Name){
        return customerRepository.getCustomerByName(Name);
    }
    // sort customer by country
    @RequestMapping(value = "api/customers/country", method = RequestMethod.GET)
    public ArrayList<CountryCount> getCustomerByCountry(){
        return customerRepository.getCustomerByCountry();
    }
    // get specific customers most popular genre by id
    @RequestMapping(value = "api/customers/customer/{id}", method = RequestMethod.GET)
    public ArrayList<CustomerGenre>  GetCustomerPopularGenre(@PathVariable int id){
        return customerRepository.GetCustomerPopularGenre(id);
    }
    // get high spender customers
    @RequestMapping(value = "api/customers/customerspender", method = RequestMethod.GET)
    public ArrayList<CustomerSpender>  GetAllCustomersByHighestSpender(){
        return customerRepository.GetAllCustomersByHighestSpender();
    }

    @RequestMapping(value = "api/customers", method = RequestMethod.POST)
    public Boolean addNewCustomer(@RequestBody Customer customer){
        return customerRepository.addCustomer(customer);
    }

    @RequestMapping(value = "api/customers/{id}", method = RequestMethod.PUT)
    public Boolean updateExistingCustomer(@PathVariable String id, @RequestBody Customer customer){
        return customerRepository.updateCustomer(customer);
    }

}
