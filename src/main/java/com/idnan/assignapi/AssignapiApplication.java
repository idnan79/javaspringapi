package com.idnan.assignapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssignapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AssignapiApplication.class, args);
	}

}
