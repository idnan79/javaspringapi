FROM openjdk:16-jdk-alpine
ADD target/thymeleafsqlite-1.jar thymeleafsqlite-1.jar
ENTRYPOINT ["java", "-jar", "thymeleafsqlite-1.jar"]